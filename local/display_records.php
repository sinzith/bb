<html>
<head>
	<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
</head>
<body>
<div id="wrapper">

<?php
include 'dbConfig.php';

$select =mysqli_query($db,"SELECT * FROM cat1");
?>

<table align="center" cellpadding="10" border="1" id="user_table">
<tr>
<th>Primary</th>

<th></th>
</tr>

<tr id="new_row">
 <td><input type="text" id="new_name"></td>
 <td><input type="button" value="Insert Row" onclick="insert_row();"></td>
</tr>
<?php
while ($row=mysqli_fetch_array($select)) 
{
 ?>
 <tr id="row<?php echo $row['p_id'];?>">
  <td id="name_val<?php echo $row['p_id'];?>"><?php echo $row['Primarycat'];?></td>
  <td>
   <input type='button' class="edit_button" id="edit_button<?php echo $row['p_id'];?>" value="edit" onclick="edit_row('<?php echo $row['p_id'];?>');">
   <input type='button' class="save_button" id="save_button<?php echo $row['p_id'];?>" value="save" onclick="save_row('<?php echo $row['p_id'];?>');" >
   <input type='button' class="delete_button" id="delete_button<?php echo $row['p_id'];?>" value="delete" onclick="delete_row('<?php echo $row['p_id'];?>');">
  </td>
 </tr>
 <?php
}
?>

</table>

</div>
</body>
</html>
<script >

	function edit_row(id)
{
 var name=document.getElementById("name_val"+id).innerHTML;
 //var age=document.getElementById("age_val"+id).innerHTML;

 document.getElementById("name_val"+id).innerHTML="<input type='text' id='name_text"+id+"' value='"+name+"'>";
 //document.getElementById("age_val"+id).innerHTML="<input type='text' id='age_text"+id+"' value='"+age+"'>";
	
 document.getElementById("edit_button"+id).style.display="none";
 document.getElementById("save_button"+id).style.display="block";
}

function save_row(id)
{
 var name=document.getElementById("name_text"+id).value;
 //var age=document.getElementById("age_text"+id).value;
	
 $.ajax
 ({
  type:'post',
  url:'modify_record.php',
  data:{
   edit_row:'edit_row',
   row_id:id,
   name_val:name
  },
  success:function(response) {
   if(response=="success")
   {
    document.getElementById("name_val"+id).innerHTML=name;
    //document.getElementById("age_val"+id).innerHTML=age;
    document.getElementById("edit_button"+id).style.display="block";
    document.getElementById("save_button"+id).style.display="none";
   }
  }
 });
}

function delete_row(id)
{
 $.ajax
 ({
  type:'post',
  url:'modify_record.php',
  data:{
   delete_row:'delete_row',
   row_id:id,
  },
  success:function(response) {
   if(response=="success")
   {
    var row=document.getElementById("row"+id);
    row.parentNode.removeChild(row);
   }
  }
 });
}

function insert_row()
{
 var name=document.getElementById("new_name").value;
 //var age=document.getElementById("new_age").value;

 $.ajax
 ({
  type:'post',
  url:'modify_record.php',
  data:{
   insert_row:'insert_row',
   name_val:name
  },
  success:function(response) {
   if(response!="")
   {
    var id=response;
    var table=document.getElementById("user_table");
    var table_len=(table.rows.length)-1;
    var row = table.insertRow(table_len).outerHTML="<tr id='row"+id+"'><td id='name_val"+id+"'>"+name+"</td><td><input type='button' class='edit_button' id='edit_button"+id+"' value='edit' onclick='edit_row("+id+");'/><input type='button' class='save_button' id='save_button"+id+"' value='save' onclick='save_row("+id+");'/><input type='button' class='delete_button' id='delete_button"+id+"' value='delete' onclick='delete_row("+id+");'/></td></tr>";

    document.getElementById("new_name").value="";
   // document.getElementById("new_age").value="";
   }
  }
 });
}
</script>