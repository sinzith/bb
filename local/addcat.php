 
 <?php 
    include 'dbConfig.php';
    //include 'dbconnect.php';
 ?>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
body {font-family: Arial;}

/* Style the tab */
.tab1{
    overflow: hidden;
    border: 1px solid #ccc;
    background-color: #f1f1f1;
}

/* Style the buttons inside the tab */
.tab1 button {
    background-color: inherit;
    float: left;
    border: none;
    outline: none;
    cursor: pointer;
    padding: 14px 16px;
    transition: 0.3s;
    font-size: 17px;
}

/* Change background color of buttons on hover */
.tab1 button:hover {
    background-color: #ddd;
}

/* Create an active/current tablink class */
.tab1 button.active {
    background-color: #ccc;
}

/* Style the tab content */
.tab1content {
    display: none;
    padding: 6px 12px;
    border: 1px solid #ccc;
    border-top: none;
    
}
</style>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

  <div class="tab1">
  <button class="tab1links" onclick="openbox1(event, 'pcat1')">Primary</button>
  <button class="tab1links" onclick="openbox1(event, 'scat1')">Secondary</button>
  <button class="tab1links" onclick="openbox1(event, 'tcat1')">Tertiary</button>
</div>

<!--PRIMAMRY CAT DISPLAY-->
<div id="pcat1" class="tab1content">
     <iframe width="500" height="300" src="display_records.php" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe><br><br>
 </div>

 <!--SECONDARY CAT DISPLAY-->

<div id="scat1" class="tab1content">
    
      <div class="container">
<?php
include 'dbconfig.php';
    $query = $db->query("SELECT * FROM cat1 where status=1 ORDER BY p_id ASC");
    $rowCount = $query->num_rows;
?>
    <form method="POST">
    <b>Primary Category :</b>
    <select id="prim" name="prim">
        <option value="">Select Primary</option>
<?php
        if($rowCount > 0){
            while($row = $query->fetch_assoc()){ 
                echo '<option value="'.$row['p_id'].'">'.$row['Primarycat'].'</option>';
            }
        }else{
            echo '<option value="">Primarycat not available</option>';
        }
?>
    </select><br><br>
    <div id="sec">
    <table align='center' cellpadding='10' border='1' id='user_table'>  </table> 
    </div>
 </form>
</div>
</div>
<script>
       $(document).ready(function(){
    $('#prim').on('change',function(){
        var primaryID = $(this).val();
        //alert(primaryID);
        if(primaryID){
            $.ajax({
                type:'POST',
                url:'ajd1.php',
                data:'p_id='+primaryID,
                success:function(html){
                    console.log(html);
                    $('#sec').html(html);
                    //$('#tertiary').html('<option value="">Select secondary first</option>'); 
                }
            }); 
        }else{
            $('#secondary').html('<option value="">Select primary first</option>');
            $('#tertiary').html('<option value="">Select secondary first</option>'); 
        }
    });
    });
</script>

<!--TERTIARY CAT DISPALY-->
<div id="tcat1" class="tab1content">
    <iframe width="500" height="440" src="tertiaryaddcat.php" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe><br><br>
</div>

<script>
function openbox1(evt, box1Name) {
    var i, tab1content, tablinks;
    tab1content = document.getElementsByClassName("tab1content");
    for (i = 0; i < tab1content.length; i++) {
        tab1content[i].style.display = "none";
    }
    tab1links = document.getElementsByClassName("tab1links");
    for (i = 0; i < tab1links.length; i++) {
        tab1links[i].className = tab1links[i].className.replace(" active", "");
    }
    document.getElementById(box1Name).style.display = "block";
    evt.currentTarget.className += " active";
}
</script>
</div>

<script>

  function edit_row(id)
{
 var name=document.getElementById("name_val"+id).innerHTML;
 //var age=document.getElementById("age_val"+id).innerHTML;

 document.getElementById("name_val"+id).innerHTML="<input type='text' id='name_text"+id+"' value='"+name+"'>";
 //document.getElementById("age_val"+id).innerHTML="<input type='text' id='age_text"+id+"' value='"+age+"'>";
  
 document.getElementById("edit_button"+id).style.display="none";
 document.getElementById("save_button"+id).style.display="block";
}

function save_row(id)
{
 var name=document.getElementById("name_text"+id).value;
 //var age=document.getElementById("age_text"+id).value;
  
 $.ajax
 ({
  type:'post',
  url:'modify_record2.php',
  data:{
   edit_row:'edit_row',
   row_id:id,
   name_val:name
  },
  success:function(response) {
   if(response=="success")
   {
    document.getElementById("name_val"+id).innerHTML=name;
    //document.getElementById("age_val"+id).innerHTML=age;
    document.getElementById("edit_button"+id).style.display="block";
    document.getElementById("save_button"+id).style.display="none";
   }
  }
 });
}

function delete_row(id)
{
 $.ajax
 ({
  type:'post',
  url:'modify_record2.php',
  data:{
   delete_row:'delete_row',
   row_id:id,
  },
  success:function(response) {
   if(response=="success")
   {
    var row=document.getElementById("row"+id);
    row.parentNode.removeChild(row);
   }
  }
 });
}

function insert_row()
{
 var name=document.getElementById("new_name").value;
 //var age=document.getElementById("new_age").value;
var ID=document.getElementById("prim").value;
//alert(ID);
 $.ajax
 ({
  type:'post',
  url:'modify_record2.php',
  data:{
   insert_row:'insert_row',
   name_val:name,p_id:ID
  },
  success:function(response) {
   if(response!="")
   {
    var id=response;
    var table=document.getElementById("user_table");
    var table_len=(table.rows.length)-1;
    var row = table.insertRow(table_len).outerHTML="<tr id='row"+id+"'>   <td id='name_val"+id+"'>"+name+"</td>  <td>   <input type='button' class='edit_button' id='edit_button"+id+"' value='edit' onclick='edit_row("+id+");'/>         <input type='button' class='save_button' id='save_button"+id+"' value='save' onclick='save_row("+id+");'/>                            <input type='button' class='delete_button' id='delete_button"+id+"' value='delete' onclick='delete_row("+id+");'/>     </td></tr>";

    document.getElementById("new_name").value="";
   // document.getElementById("new_age").value="";
   }
  }
 });
}
</script>